package com.example.demo.sales.rest;

import com.example.demo.DemoApplication;
import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@Sql(scripts="/plants-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SalesRestControllerTests {
    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testGetAllPlants() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2019-04-14&endDate=2019-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() { });

        assertThat(plants.size()).isEqualTo(4);

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2019, 4, 25)));

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testPurchaseOrderAcceptance() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/sales/plants?name=Exc&startDate=2019-04-14&endDate=2019-04-25"))
                .andReturn();
        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(2));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2019, 4, 25)));

        result = mockMvc.perform(post("/api/sales/orders")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        assertThat(order.get_xlink("allocate")).isNotNull();

        mockMvc.perform(post(order.get_xlink("allocate").getHref()))
                .andReturn();
    }

    @Test
    public void testGetPurchaseOrderAvailableItems() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders/1/items"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("does not exist"));

        result = mockMvc.perform(get("/api/sales/plants?name=Mini&startDate=2019-04-14&endDate=2019-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2019, 4, 25)));

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        result = mockMvc.perform(get("/api/sales/orders/1/items"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryItemDTO> items = mapper.readValue(result.getResponse().getContentAsString(),new TypeReference<List<PlantInventoryItemDTO>>() { });
        assertThat(items.size()).isEqualTo(2);
    }

    @Test
    public void testFindPurchaseOrders() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/orders?state=pending"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PurchaseOrderDTO> pos = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PurchaseOrderDTO>>() { });
        assertThat(pos.size()).isEqualTo(0);

        MvcResult resultPlants = mockMvc.perform(
                get("/api/sales/plants?name=Exc&startDate=2019-04-14&endDate=2019-04-25"))
                .andReturn();

        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(resultPlants.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 1), LocalDate.of(2019, 4, 12)));

        PurchaseOrderDTO order2 = new PurchaseOrderDTO();
        order2.setPlant(plants.get(2));
        order2.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 1), LocalDate.of(2019, 4, 12)));

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order2)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        result = mockMvc.perform(get("/api/sales/orders?state=pending"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        pos = mapper.readValue(result.getResponse().getContentAsString(),new TypeReference<List<PurchaseOrderDTO>>() { });
        assertThat(pos.size()).isEqualTo(2);
    }

    @Test
    public void testClosePurchaseOrders() throws Exception {
        mockMvc.perform(delete("/api/sales/orders/1"))
                .andExpect(status().isNotFound());

        MvcResult resultPlants = mockMvc.perform(get("/api/sales/plants?name=Mini&startDate=2019-06-14&endDate=2019-06-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(resultPlants.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });
        PlantInventoryEntryDTO plantInventoryEntryDTO = plants.get(0);
        PurchaseOrderDTO newPo = createNewPo(plantInventoryEntryDTO);

        mockMvc.perform(delete("/api/sales/orders/" + newPo.get_id()))
                .andExpect(status().isConflict());

        PlantInventoryItemDTO item = new PlantInventoryItemDTO();
        item.set_id(plantInventoryEntryDTO.get_id());
        String urlTemplate = String.format("/api/sales/orders/%s/allocate", newPo.getPlant().get_id());

        PurchaseOrderDTO allocatedPo = mapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post(urlTemplate)
                .content(mapper.writeValueAsString(item))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                .andReturn().getResponse().getContentAsString(), PurchaseOrderDTO.class);

        mockMvc.perform(delete("/api/sales/orders/" + allocatedPo.get_id()))
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + allocatedPo.get_id()))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        PurchaseOrderDTO po = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertThat(po.getStatus()).isEqualByComparingTo(POStatus.CLOSED);
    }

    @Test
    public void testRejectPurchaseOrder() throws Exception {
        mockMvc.perform(delete("/api/sales/orders/1/reject"))
                .andExpect(status().isNotFound());

        MvcResult resultPlants = mockMvc.perform(get("/api/sales/plants?name=Mini&startDate=2019-06-14&endDate=2019-06-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(resultPlants.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });


        PlantInventoryEntryDTO plantInventoryEntryDTO = plants.get(0);
        PurchaseOrderDTO order = createNewPo(plantInventoryEntryDTO);
        PlantInventoryEntryDTO plantInventoryEntryDTOSecond = plants.get(1);
        PurchaseOrderDTO order2 = createNewPo(plantInventoryEntryDTOSecond);


        mockMvc.perform(delete("/api/sales/orders/" + order.get_id() + "/reject"))
                .andExpect(status().isOk());

        PlantInventoryItemDTO item = new PlantInventoryItemDTO();
        item.set_id(plantInventoryEntryDTOSecond.get_id());
        String urlTemplate = String.format("/api/sales/orders/%s/allocate", order2.getPlant().get_id());

        PurchaseOrderDTO allocatedPo = mapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post(urlTemplate)
                .content(mapper.writeValueAsString(item))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                .andReturn().getResponse().getContentAsString(), PurchaseOrderDTO.class);

        mockMvc.perform(delete("/api/sales/orders/" + allocatedPo.get_id() + "/reject"))
                .andExpect(status().isConflict());


        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + order.get_id()))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        PurchaseOrderDTO po = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
        assertThat(po.getStatus()).isEqualByComparingTo(POStatus.REJECTED);
    }

    @Test
    public void testNewlyCreatedPO() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Mini&startDate=2019-06-14&endDate=2019-06-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(0));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 6, 14), LocalDate.of(2019, 6, 25)));

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        result = mockMvc.perform(get("/api/sales/orders/1"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        PurchaseOrderDTO po = mapper.readValue(result.getResponse().getContentAsString(),new TypeReference<PurchaseOrderDTO>() { });

        assertThat(po.getPlant().get_id()).isNotNull();
        assertThat(po.getStatus()).isEqualTo(POStatus.PENDING);
        assertThat(po.get_id()).isNotNull();

        BusinessPeriodDTO rentalPeriod = po.getRentalPeriod();
        assertThat(rentalPeriod.getStartDate().isBefore(LocalDate.now()));
        assertThat(rentalPeriod.getEndDate().isBefore(LocalDate.now()));
        assertThat(rentalPeriod.getStartDate().isBefore(rentalPeriod.getEndDate()));
        assertThat(rentalPeriod.getStartDate()).isEqualTo(LocalDate.of(2019, 6, 14));
        assertThat(rentalPeriod.getEndDate()).isEqualTo(LocalDate.of(2019, 6, 25));
    }

    @Test
    public void testCreatePurchaseOrder() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/sales/plants?name=Exc&startDate=2019-04-14&endDate=2019-04-25"))
                .andReturn();
        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(0));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2019, 4, 25)));

        PurchaseOrderDTO order2 = new PurchaseOrderDTO();
        order2.setPlant(plants.get(1));
        order2.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2019, 4, 25)));

        order = mapper.readValue(
                (mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/sales/orders")
                        .content(mapper.writeValueAsString(order))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isCreated())
                        .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                        .andReturn()).getResponse().getContentAsString(), PurchaseOrderDTO.class);

        assertThat(order.get_id()).isNotNull();
        assertThat(order.get_id()).isEqualTo(1);
        assertThat(order.getStatus()).isEqualTo(POStatus.PENDING);

        order2 = mapper.readValue(
                (mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/sales/orders")
                        .content(mapper.writeValueAsString(order2))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isCreated())
                        .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                        .andReturn()).getResponse().getContentAsString(), PurchaseOrderDTO.class);

        assertThat(order2.get_id()).isNotNull();
        assertThat(order2.get_id()).isEqualTo(2);
        assertThat(order2.getStatus()).isEqualTo(POStatus.PENDING);

        PurchaseOrderDTO acceptOrder = mapper.readValue(
                (mockMvc.perform(post("/api/sales/orders/1/accept")
                        .content(mapper.writeValueAsString(order))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isCreated())
                        .andExpect(jsonPath("$._id").exists())
                        .andReturn()).getResponse().getContentAsString(), PurchaseOrderDTO.class);

        PurchaseOrderDTO rejectOrder = mapper.readValue(
                (mockMvc.perform(delete("/api/sales/orders/2/reject")
                        .content(mapper.writeValueAsString(order2))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$._id").exists())
                        .andReturn()).getResponse().getContentAsString(), PurchaseOrderDTO.class);

        assertThat(rejectOrder.get_id()).isNotNull();
        assertThat(rejectOrder.get_id()).isEqualTo(2);
        assertThat(rejectOrder.getStatus()).isEqualTo(POStatus.REJECTED);

        mockMvc.perform(post("/api/sales/orders/2/accept")
                .content(mapper.writeValueAsString(order2))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$._id").doesNotExist());

        mockMvc.perform(delete("/api/sales/orders/1/reject")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$._id").doesNotExist());

        assertThat(acceptOrder.get_id()).isNotNull();
        assertThat(acceptOrder.get_id()).isEqualTo(1);
        assertThat(acceptOrder.getStatus()).isEqualTo(POStatus.OPEN);
    }

    @Test
    public void testCreatePurchaseOrderWithInvalidDates() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/sales/plants?name=Exc&startDate=2019-04-14&endDate=2019-04-25"))
                .andReturn();
        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO failDateOrder = new PurchaseOrderDTO();
        failDateOrder.setPlant(plants.get(2));
        failDateOrder.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2017, 4, 25)));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sales/orders")
                .content(mapper.writeValueAsString(failDateOrder))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());

        assertThat(failDateOrder.get_id()).isNull();
    }

    @Test
    public void testOpenAndClosePurchaseOrders() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Mini&startDate=2019-06-14&endDate=2019-06-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();
        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });
        PlantInventoryEntryDTO plantInventoryEntryDTO = plants.get(0);
        PurchaseOrderDTO newPo = createNewPo(plantInventoryEntryDTO);

        PlantInventoryItemDTO item = new PlantInventoryItemDTO();
        item.set_id(plantInventoryEntryDTO.get_id());
        String urlTemplate = String.format("/api/sales/orders/%s/allocate", newPo.getPlant().get_id());

        PurchaseOrderDTO allocatedPo = mapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post(urlTemplate)
                .content(mapper.writeValueAsString(item))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                .andReturn().getResponse().getContentAsString(), PurchaseOrderDTO.class);

        PurchaseOrder purchaseOrder = poRepository.findById(allocatedPo.get_id()).orElse(null);
        List<PlantReservation> plantReservations = poRepository.findPlantReservations(purchaseOrder.getId());
        assertThat(purchaseOrder).isNotNull();

        assertThat(plantReservations.size()).isEqualTo(1);
        assertThat(plantReservations.get(0).getSchedule()).isEqualTo(purchaseOrder.getRentalPeriod());
        assertThat(purchaseOrder.getStatus()).isEqualTo(POStatus.OPEN);
        assertThat(plantReservations.get(0).getPlant().getId()).isEqualTo(newPo.getPlant().get_id());

        PurchaseOrderDTO closedPO = mapper.readValue(mockMvc.perform(delete("/api/sales/orders/" + String.valueOf(purchaseOrder.getId())))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), PurchaseOrderDTO.class);
        PurchaseOrder closedPurchaseOrder = poRepository.findById(closedPO.get_id()).orElse(null);
        List<PlantReservation> closedPoPlantReservation = poRepository.findPlantReservations(closedPO.get_id());

        assertThat(closedPO.getStatus()).isEqualTo(POStatus.CLOSED);
        assertThat(closedPoPlantReservation.get(0).getSchedule().getStartDate()).isEqualTo(closedPO.getRentalPeriod().getStartDate());
        assertThat(closedPoPlantReservation.get(0).getSchedule().getEndDate()).isEqualTo(closedPO.getRentalPeriod().getEndDate());
        assertThat(purchaseOrder.getTotal().compareTo(new BigDecimal(0L))).isGreaterThan(0);
        assertThat(closedPurchaseOrder.getTotal().compareTo(new BigDecimal(0L))).isGreaterThan(0);
    }

    private PurchaseOrderDTO createNewPo(PlantInventoryEntryDTO plantInventoryEntryDTO) throws Exception{
        PurchaseOrderDTO po = new PurchaseOrderDTO();
        po.setPlant(plantInventoryEntryDTO);
        po.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 4, 14), LocalDate.of(2019, 4, 25)));

        PurchaseOrderDTO purchaseOrderDTO = mapper.readValue(
                (mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/sales/orders")
                        .content(mapper.writeValueAsString(po))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isCreated())
                        .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                        .andReturn()).getResponse().getContentAsString(), PurchaseOrderDTO.class);
        return purchaseOrderDTO;
    }

    @Test
    public void testAllocatePurchaseOrderCorrect() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/sales/plants?name=Exc&startDate=2019-06-14&endDate=2019-06-25"))
                .andReturn();
        List<PlantInventoryEntryDTO> plants =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() { });

        PurchaseOrderDTO po = new PurchaseOrderDTO();
        po.setPlant(plants.get(1));
        po.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2019, 6, 14), LocalDate.of(2019, 6, 25)));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sales/orders")
                .content(mapper.writeValueAsString(po))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        result = mockMvc.perform(get("/api/sales/orders/1"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        PurchaseOrderDTO createdPo = mapper.readValue(result.getResponse().getContentAsString(),new TypeReference<PurchaseOrderDTO>() { });
        assertThat(createdPo.get_id()).isNotNull();

        result = mockMvc.perform(get(String.format("/api/sales/orders/%s/items", createdPo.get_id())))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryItemDTO> items = mapper.readValue(result.getResponse().getContentAsString(),new TypeReference<List<PlantInventoryItemDTO>>() { });

        PlantInventoryItemDTO item = new PlantInventoryItemDTO();
        item.set_id(items.get(0).get_id());
        String requestString = String.format("/api/sales/orders/%s/allocate", createdPo.get_id());

        PurchaseOrderDTO allocatedPo = mapper.readValue(mockMvc.perform(MockMvcRequestBuilders
                .post(requestString)
                .content(mapper.writeValueAsString(item))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._id").exists())
                .andReturn().getResponse().getContentAsString(), PurchaseOrderDTO.class);

        PurchaseOrder purchaseOrder = poRepository.findById(allocatedPo.get_id()).orElse(null);

        assertThat(purchaseOrder).isNotNull();
        List<PlantReservation> plantReservations = poRepository.findPlantReservations(purchaseOrder.getId());
        assertThat(plantReservations.size()).isEqualTo(1);
        assertThat(purchaseOrder.getStatus()).isEqualTo(POStatus.OPEN);
        assertThat(purchaseOrder.getRentalPeriod().getStartDate()).isEqualTo(LocalDate.of(2019, 6, 14));
        assertThat(purchaseOrder.getRentalPeriod().getEndDate()).isEqualTo(LocalDate.of(2019, 6, 25));
    }

    @Test
    public void testAllocatePurchaseOrderCorrectIncorrect() throws Exception {
        PlantInventoryItemDTO item = new PlantInventoryItemDTO();
        item.set_id(1L);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sales/orders/0/allocate")
                .content(mapper.writeValueAsString(item))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("PO cannot be allocated"));

        item.set_id(0L);

        result = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sales/orders/0/allocate")
                .content(mapper.writeValueAsString(item))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("PO cannot be allocated"));
    }
}

