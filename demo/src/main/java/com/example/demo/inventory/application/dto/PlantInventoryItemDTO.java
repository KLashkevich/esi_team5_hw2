package com.example.demo.inventory.application.dto;

import com.example.demo.common.rest.ResourceSupport;
import com.example.demo.inventory.domain.model.EquipmentCondition;
import lombok.Data;

@Data
public class PlantInventoryItemDTO extends ResourceSupport {
    Long _id;
    String serialNumber;
    EquipmentCondition equipmentCondition;
}
