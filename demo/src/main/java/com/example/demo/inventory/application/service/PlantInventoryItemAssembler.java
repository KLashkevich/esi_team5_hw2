package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.rest.InventoryRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryItemAssembler
        extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {
    public PlantInventoryItemAssembler () {
        super(InventoryRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem item) {
        PlantInventoryItemDTO dto = createResourceWithId(item.getId(), item);
        dto.set_id(item.getId());
        dto.setSerialNumber(item.getSerialNumber());
        dto.setEquipmentCondition(item.getEquipmentCondition());
        return dto;
    }
}
