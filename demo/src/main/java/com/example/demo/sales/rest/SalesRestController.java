package com.example.demo.sales.rest;

import com.example.demo.common.application.exception.PlantNotFoundException;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/sales")
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        return inventoryService.findAvailablePlants(plantName.toLowerCase(), startDate, endDate);
    }

    @GetMapping("/orders")
    public ResponseEntity findPurchaseOrders(@RequestParam(name = "status", required = false) String status) {
        try {
            List<PurchaseOrderDTO> orders = salesService.findPurchaseOrders(status);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(orders);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @GetMapping("/orders/{id}/items")
    public ResponseEntity getPurchaseOrderAvailableItems(@PathVariable("id") Long id) {
        try {
            PurchaseOrderDTO po = salesService.findPO(id);

            List<PlantInventoryItemDTO> items = inventoryService.findAvailableItems(po);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(items);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity fetchPurchaseOrder(@PathVariable("id") Long id) {
        try {
            PurchaseOrderDTO po = salesService.findPO(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(po);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @PostMapping("/orders")
    public ResponseEntity createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws Exception {
        try {
            PurchaseOrderDTO newlyCreatePODTO = salesService.createPO(partialPODTO);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newlyCreatePODTO.getId().getHref()));
            // The above line won't working until you update PurchaseOrderDTO to extend ResourceSupport

            return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/orders/{id}/accept")
    public ResponseEntity acceptPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(salesService.acceptPO(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/orders/{poId}/allocate")
    public ResponseEntity allocatePurchaseOrder(@PathVariable Long poId, @RequestBody PlantInventoryItemDTO item ) throws Exception {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(salesService.allocatePO(poId, item.get_id()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR:" + ex.getMessage());
        }
    }

    @DeleteMapping("/orders/{id}/reject")
    public ResponseEntity rejectPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(salesService.rejectPO(id));
        } catch (Exception ex) {
            HttpStatus status = HttpStatus.CONFLICT;
            if (ex.getMessage().toLowerCase().contains("not found"))
                status = HttpStatus.NOT_FOUND;
            return ResponseEntity
                    .status(status)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity closePurchaseOrder(@PathVariable Long id)  {
        try {
            PurchaseOrderDTO po = salesService.closePO(id);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(po);
        } catch (Exception ex) {
            String msg = ex.getMessage();
            HttpStatus status = HttpStatus.CONFLICT;
            if (msg.toLowerCase().contains("not found"))
                status = HttpStatus.NOT_FOUND;
            return ResponseEntity
                    .status(status)
                    .body("ERROR: " + msg);
        }
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
    }
}
