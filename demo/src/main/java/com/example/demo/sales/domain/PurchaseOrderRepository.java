package com.example.demo.sales.domain;

import com.example.demo.inventory.domain.model.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    @Query(value="select * from purchase_order where UPPER(status) like %?1%", nativeQuery=true)
    List<PurchaseOrder> findPurchaseOrdersByStatus(String status);

    @Query(value="Select p.reservations from PurchaseOrder p where p.id = ?1")
    List<PlantReservation> findPlantReservations(Long id);
}
