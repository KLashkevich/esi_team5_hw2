package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    PurchaseOrderValidator purchaseOrderValidator;

    public PurchaseOrderDTO findPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if (po == null) {
            throw new Exception(String.format("Purchase Order with id %s does not exist", id));
        }
        return purchaseOrderAssembler.toResource(po);
    }

    public List<PurchaseOrderDTO> findPurchaseOrders (String status) throws Exception {
        List <PurchaseOrder> orders;

        if(status != null && !status.isEmpty()) {
            if (isPurchaseOrderState(status)) {
                orders = poRepository.findPurchaseOrdersByStatus(status.toUpperCase());
            }
            else {
                throw new Exception(String.format("Incorrect Status '%s'", status));
            }
        }
        else {
            orders = poRepository.findAll();
        }

        return purchaseOrderAssembler.toResources(orders);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                poDTO.getRentalPeriod().getStartDate(),
                poDTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid PO Period");

        if(poDTO.getPlant() == null)
            throw new Exception("Invalid Input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        if(plant == null)
            throw new Exception("Plant NOT Found");

        PurchaseOrder po = PurchaseOrder.of(plant, period);
        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        poRepository.save(po);

        return purchaseOrderAssembler.toResource(po);

    }

    public PurchaseOrderDTO acceptPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        po.setStatus(POStatus.OPEN);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO rejectPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.REJECTED);
        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO closePO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if (po == null)
            throw new Exception("PO Not Found");
        else if(po.getStatus() != POStatus.OPEN)
            throw new Exception("PO cannot be closed due to it is not being Open");
        po.setStatus(POStatus.CLOSED);
        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO allocatePO(Long poId, Long itemID) throws Exception {
        PurchaseOrder purchaseOrder = poRepository.findById(poId).orElse(null);
        PlantInventoryItem itemById = inventoryRepository.findItemById(itemID);
        if( validatePoAllocateItem(purchaseOrder, itemById)){
            DataBinder beforeOpening = new DataBinder(purchaseOrder);
            beforeOpening.addValidators(purchaseOrderValidator);
            beforeOpening.validate();
            if(beforeOpening.getBindingResult().hasErrors()){
                throw new Exception (beforeOpening.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
            }

            PlantReservation plantReservation = new PlantReservation();
            plantReservation.setSchedule(purchaseOrder.getRentalPeriod());
            plantReservation.setPlant(itemById);
            purchaseOrder.setStatus(POStatus.OPEN);
            purchaseOrder.addReservation(plantReservation);

            long daysToRentFor = DAYS.between(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate());
            BigDecimal totalPrice = itemById.getPlantInfo().getPrice().multiply(new BigDecimal(daysToRentFor));
            purchaseOrder.setTotal(totalPrice);

            DataBinder dataBinder = new DataBinder(purchaseOrder);
            dataBinder.addValidators(purchaseOrderValidator);
            dataBinder.validate();
            if(dataBinder.getBindingResult().hasErrors()){
                throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
            }
            plantReservationRepository.save(plantReservation);
            poRepository.save(purchaseOrder);
            return purchaseOrderAssembler.toResource(purchaseOrder);
        }
        throw new Exception ("PO cannot be allocated to this plant");
    }

    private boolean isPurchaseOrderState (String status) {
        for (POStatus s : POStatus.values()) {
            if (s.name().contains(status.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    private PurchaseOrder getPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null)
            throw new Exception("PO Not Found");
        if(po.getStatus() != POStatus.PENDING)
            throw new Exception("PO cannot be accepted/rejected due to it is not Pending");
        return po;
    }

    boolean validatePoAllocateItem(PurchaseOrder po, PlantInventoryItem itemById){
        if(po != null && itemById != null){
            List<PlantInventoryItem> availableItems =
                    inventoryRepository.findAvailableItems(po.getPlant().getId(), po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate());
            return availableItems.stream().filter(item -> Objects.equals(item.getId(), itemById.getId())).findFirst().orElse(null) != null && po.getStatus() == POStatus.PENDING;
        }
        return false;
    }
}
